## Package Boilerlplate

Basic package boilerlplate for modular npm-packages

# Steps to create a library:

1. Create modular app structure in *src*
1. Rename your library in **package.json** of your library
1. Add **package.json** files for parts of the app, which could be used separately to *parts* folder
1. Add typings for each part
1. For better experience, you should add root *index.js* file, which will export all the parts and 
a root *index.d.ts* file, which will export all the typings
1. Update *README* to describe your package
1. Create some examples to show how your package can be used, to add library as a dependency for the example:
  2. Add `"library-name": "link:../../"` to the dependencies of your example
  2. Run `yarn`

# Using library
You can import either whole library: `import * as lib from 'library-name;`, or a separated part of the library: `import featureOne from 'library-name/parts/feature-one';`

# Building:
Run `yarn build` to prepare your library to be deployed

# Releasing:
Run `yarn release:$type` to release your app (where **$type** can be: patch, minor or major)

# Development
Run `yarn start` to start your application
To support run-time example's updating you should import the whole library rather than a part