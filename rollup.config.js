import fs from 'fs';
import { resolve, join } from 'path';
import babel from 'rollup-plugin-babel';
import nodeResolve from 'rollup-plugin-node-resolve';
import { eslint } from 'rollup-plugin-eslint';

const SOURCE_DIR = 'src';
const DEST_DIR = 'es';
const INDEX = 'index.js';

// Compiling all parts

export default fs.readdirSync(SOURCE_DIR).map(entry => {
  const entryPath = join(entry, entry.endsWith(INDEX) ? '' : INDEX);

  return {
    input: resolve(SOURCE_DIR, entryPath),
    output: {
      format: 'es',
      file: resolve(DEST_DIR, entryPath),
    },
    plugins: [
      nodeResolve({
        jsnext: true,
      }),
      eslint(),
      babel({
        exclude: 'node_modules/**',
      }),
    ],
  };
});
